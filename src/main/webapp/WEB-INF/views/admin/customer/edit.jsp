<%@include file="../shared/header.jsp"%>
<form method="post" action="${BASE_URL}/admin/customer/save">
    <div class="form-group">
        <label>First Name</label>
        <input type="text" name="firstName" class="form-control" value="${customer.firstName}" required/>

    </div>
    <div class="form-group">
        <label>Last Name</label>
        <input type="text" name="lastName" class="form-control"  value="${customer.lastName}" required/>

    </div>
    <div class="form-group">
        <label>Email address</label>
        <input type="email" name="email" class="form-control" value="${customer.email}" required/>
    </div>
    <div class="form-group">
        <label>Contact Number</label>
        <input type="text" name="contactNo" class="form-control"  value="${customer.contactNo}"required/>
    </div>
    <div class="checkbox">
        <label>Status</label>
        <label><input type="checkbox" name="status" <c:if test="${customer.status}"> checked="checked"</c:if> required/>Active</label>
        </div>
        <input type="hidden" name="id" value="${customer.id}"/>
    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
    <button type="submit" class="btn btn-success">Save</button>
    <a href="${BASE_URL}/admin/customer" class="btn btn-danger">
        Back
    </a>
</div>


</form>

<%@include file="../shared/footer.jsp"%>