<%@include file="../shared/header.jsp"%>
<form method="post" action="${BASE_URL}/admin/customer/saveFollowUp">
    
    <div class="form-group">
        <label>Message</label>
        <input type="text" name="message" class="form-control" required/>

    </div>
    <div class="form-group">
        <label>Follow Up Date</label>
        <input type="date" name="followDate" class="form-control"  required/>
    </div>
    <div class="form-group">
        <label>Next Follow Up Date</label>
        <input type="date" name="nextFollowDate" class="form-control" required/>
    </div>
        <input type="hidden" name="customer.id" value="${customer.id}"/>
    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
    <button type="submit" class="btn btn-success">Save</button>
    <a href="${BASE_URL}/admin/customer" class="btn btn-danger">
        Back
    </a>
</div>


</form>

<%@include file="../shared/footer.jsp"%>