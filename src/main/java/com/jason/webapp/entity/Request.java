/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jason.webapp.entity;

/**
 *
 * @author Prajwal
 */
public class Request {
    private int id;
    private Customer customer;
    private String requestMessage;

    public Request() {
    }

    public Request(int id, Customer customer, String requestMessage) {
        this.id = id;
        this.customer = customer;
        this.requestMessage = requestMessage;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public String getRequestMessage() {
        return requestMessage;
    }

    public void setRequestMessage(String requestMessage) {
        this.requestMessage = requestMessage;
    }
    
    
}
