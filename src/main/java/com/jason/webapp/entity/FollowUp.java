/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jason.webapp.entity;

import java.sql.Date;

/**
 *
 * @author Prajwal
 */
public class FollowUp {
    private int id;
    private Customer customer;
    private String message;
    private Date followDate,nextFollowDate;

    public FollowUp() {
    }

    public FollowUp(int id, Customer customer, String message, Date followDate, Date nextFollowDate) {
        this.id = id;
        this.customer = customer;
        this.message = message;
        this.followDate = followDate;
        this.nextFollowDate = nextFollowDate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Date getFollowDate() {
        return followDate;
    }

    public void setFollowDate(Date followDate) {
        this.followDate = followDate;
    }

    public Date getNextFollowDate() {
        return nextFollowDate;
    }

    public void setNextFollowDate(Date nextFollowDate) {
        this.nextFollowDate = nextFollowDate;
    }

    @Override
    public String toString() {
        return "FollowUp{" + "id=" + id + ", customer=" + customer + ", message=" + message + ", followDate=" + followDate + ", nextFollowDate=" + nextFollowDate + '}';
    }
    
    
}
