/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jason.webapp.controller.admin;

import com.jason.webapp.entity.Customer;
import com.jason.webapp.entity.FollowUp;
import com.jason.webapp.service.CustomerServiceDAO;
import com.jason.webapp.service.FollowUpServiceDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author Prajwal
 */
@Controller
@RequestMapping(value = "/admin/customer")
public class CustomerController {

    @Autowired
    private CustomerServiceDAO customerService;//customerServiceDAOimpl ko naam(value) customerService cha so yo garna paiyo
    @Autowired
    private FollowUpServiceDAO followUpService;

    @RequestMapping(method = RequestMethod.GET)
    public String index(Model model) {
        try {
            model.addAttribute("customers", customerService.getAll());
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return "admin/customer/index";
    }

    @RequestMapping(value = "/add", method = RequestMethod.GET)
    public String add() {
        return "admin/customer/add";
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public String save(@ModelAttribute("Customer") Customer customer) {
        try {
            customerService.insert(customer);
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return "redirect:/admin/customer";
    }

    @RequestMapping(value = "/edit/{id}", method = RequestMethod.GET)
    public String edit(Model model, @PathVariable("id") int id) {
        try {
            model.addAttribute("customer", customerService.getById(id));
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return "admin/customer/edit";
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public String update(@ModelAttribute("Customer") Customer customer) {
        try {
            if (customer.getId() > 0) {
                customerService.update(customer);
            } else {
                customerService.insert(customer);
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return "redirect:/admin/customer?success&action=save";
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
    public String delete(@PathVariable("id") int id) {
        try {
            customerService.delete(id);
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return "redirect:/admin/customer";
    }

    @RequestMapping(value = "/addFollowUp/{id}", method = RequestMethod.GET)
    public String addFollowUp(Model model, @PathVariable("id") int id) {
        try {
            model.addAttribute("customer", customerService.getById(id));
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return "admin/customer/addFollowup";
    }

    @RequestMapping(value = "/saveFollowUp", method = RequestMethod.POST)
    public String saveFollowUp(@ModelAttribute("FollowUp") FollowUp followUp) {
        try {

            followUpService.insert(followUp);
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }

        return "redirect:/admin/customer?success&action=save";
    }
}
