/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jason.webapp.dao.impl;

import com.jason.webapp.dao.FollowUpDAO;
import com.jason.webapp.entity.Customer;
import com.jason.webapp.entity.FollowUp;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Prajwal
 */
//@Component or repository 
@Repository(value = "followUpDAO")
public class FollowUpDAOImpl implements FollowUpDAO {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public List<FollowUp> getAll() throws ClassNotFoundException, SQLException {
        String sql = "SELECT * FROM followup fup JOIN customers cust on fup.customer_id=cust.customer_id";
        return jdbcTemplate.query(sql, new RowMapper<FollowUp>() {

            @Override
            public FollowUp mapRow(ResultSet rs, int i) throws SQLException {

                FollowUp f = new FollowUp();
                f.setId(rs.getInt("id"));
                f.setMessage(rs.getString("message"));
                f.setFollowDate(rs.getDate("follow_date"));
                f.setNextFollowDate(rs.getDate("next_follow_date"));
                Customer c = new Customer(rs.getInt("customer_id"), rs.getString("first_name"), rs.getString("last_name"), rs.getString("email"), rs.getString("contact_no"), rs.getBoolean("status"));
                f.setCustomer(c);
                return f;
            }
        });
    }

    @Override
    public FollowUp getById(int id) throws ClassNotFoundException, SQLException {
        String sql = "SELECT * FROM followup WHERE id=?";
        return jdbcTemplate.queryForObject(sql, new Object[]{id}, new RowMapper<FollowUp>() {

            @Override
            public FollowUp mapRow(ResultSet rs, int i) throws SQLException {

                FollowUp f = new FollowUp();
                f.setId(rs.getInt("id"));
                f.setMessage(rs.getString("message"));
                f.setFollowDate(rs.getDate("follow_date"));
                f.setNextFollowDate(rs.getDate("next_follow_date"));
                Customer c = new Customer(rs.getInt("customer_id"), rs.getString("first_name"), rs.getString("last_name"), rs.getString("email"), rs.getString("contact_no"), rs.getBoolean("status"));
                f.setCustomer(c);
                return f;

            }
        });
    }

    @Override
    public int insert(FollowUp f) throws ClassNotFoundException, SQLException {
        String sql = "INSERT INTO followup(customer_id,message,follow_date,next_follow_date) VALUES(?,?,?,?)";
        return jdbcTemplate.update(sql, new Object[]{
            f.getCustomer().getId(),f.getMessage(), f.getFollowDate(), f.getNextFollowDate()
        });
    }

    @Override
    public int update(FollowUp f) throws ClassNotFoundException, SQLException {
        String sql = "UPDATE followup SET customer_id=?,follow_date=?,next_follow_date=? WHERE id=?";
        return jdbcTemplate.update(sql, new Object[]{
            f.getCustomer().getId(),f.getMessage(), f.getFollowDate(), f.getNextFollowDate(),
            f.getId()
        });
    }

    @Override
    public int delete(int id) throws ClassNotFoundException, SQLException {
        String sql = "DELETE FROM followup WHERE id=?";
        return jdbcTemplate.update(sql, new Object[]{
            id
        });
    }
}
