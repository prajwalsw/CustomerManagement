/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jason.webapp.service;

import com.jason.webapp.entity.FollowUp;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author Prajwal
 */
public interface FollowUpServiceDAO {
    List<FollowUp> getAll() throws ClassNotFoundException, SQLException;

    FollowUp getById(int id) throws ClassNotFoundException, SQLException;

    int insert(FollowUp f) throws ClassNotFoundException, SQLException;

    int update(FollowUp f) throws ClassNotFoundException, SQLException;

    int delete(int id) throws ClassNotFoundException, SQLException;
    
}
