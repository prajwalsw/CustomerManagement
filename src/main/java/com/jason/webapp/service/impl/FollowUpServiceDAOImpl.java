/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jason.webapp.service.impl;


import com.jason.webapp.dao.FollowUpDAO;
import com.jason.webapp.entity.FollowUp;
import com.jason.webapp.service.FollowUpServiceDAO;
import java.sql.SQLException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Prajwal
 */
@Service(value="followUpService")
public class FollowUpServiceDAOImpl implements FollowUpServiceDAO{
    @Autowired
    private FollowUpDAO followUpDAO;//followUpDAOimpl maa repository ko naam followUpDAO cha.. so paiyoo
    @Override
    public List<FollowUp> getAll() throws ClassNotFoundException, SQLException {
        return followUpDAO.getAll();
    }

    @Override
    public FollowUp getById(int id) throws ClassNotFoundException, SQLException {
        return followUpDAO.getById(id);
    }

    @Override
    public int insert(FollowUp f) throws ClassNotFoundException, SQLException {
        return followUpDAO.insert(f);
    }

    @Override
    public int update(FollowUp f) throws ClassNotFoundException, SQLException {
        return followUpDAO.update(f);
    }

    @Override
    public int delete(int id) throws ClassNotFoundException, SQLException {
        return followUpDAO.delete(id);
    }
    
}
