/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jason.webapp.service.impl;

import com.jason.webapp.dao.CustomerDAO;
import com.jason.webapp.entity.Customer;
import com.jason.webapp.service.CustomerServiceDAO;
import java.sql.SQLException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Prajwal
 */
@Service(value="customerService")
public class CustomerServiceDAOImpl implements CustomerServiceDAO{
    @Autowired
    private CustomerDAO customerDAO;//customerDAOimpl maa repository ko naam customerDAO cha.. so paiyoo
    @Override
    public List<Customer> getAll() throws ClassNotFoundException, SQLException {
        return customerDAO.getAll();
    }

    @Override
    public Customer getById(int id) throws ClassNotFoundException, SQLException {
        return customerDAO.getById(id);
    }

    @Override
    public int insert(Customer c) throws ClassNotFoundException, SQLException {
        return customerDAO.insert(c);
    }

    @Override
    public int update(Customer c) throws ClassNotFoundException, SQLException {
        return customerDAO.update(c);
    }

    @Override
    public int delete(int id) throws ClassNotFoundException, SQLException {
        return customerDAO.delete(id);
    }
    
}
