<%@include file="../shared/header.jsp"%>

 <%@include file="../frontend/navs.jsp"%>

  <h1>List of Customers</h1>
  <div>
      <c:if test="${param.action!=null}">
          <c:if test="${param.success!=null}">
              <div class="alert alert-success" role="alert">
                  Successfully Updated.
              </div> 
          </c:if>
      </c:if>
  </div>
            <div class="pull-right">
                <p>
                    <a href="${BASE_URL}/admin/customer/add" class="btn btn-primary">
                        Add 
                       
                    </a>
                </p>
            </div>
            <table class="table table-bordered table-hover table-striped">
                <tr>
                    <th>Id</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Contact No</th>
                    <th>Status</th>
                    <th>Follow Up</th>
                    <th>Action</th>
                </tr>
                <c:forEach var="c" items="${customers}">
                    <tr>
                        <td>${c.id}</td>
                        <td>${c.firstName} ${c.lastName}</td>
                        <td>${c.email}</td>
                        <td>${c.contactNo}</td>
                        <td>${c.status}</td>
                        <td>  <a href="${BASE_URL}/admin/customer/addFollowUp/${c.id}" class="btn btn-xs btn-default">
                               Add
                            </a>
                            <a href="${BASE_URL}/admin/customer/viewFollowUp/${c.id}" class="btn btn-xs btn-default">
                                View
                            </a>
                        </td>
                        <td>
                             <a href="${BASE_URL}/admin/customer/edit/${c.id}" class="btn btn-xs btn-success">
                                <span class="glyphicon glyphicon-pencil"/>
                            </a>
                            <a href="${BASE_URL}/admin/customer/delete/${c.id}" class="btn btn-xs btn-danger" onclick="return confirm('Are you sure to delete?')">
                                <span class="glyphicon glyphicon-trash"/>
                            </a>
                           
                        </td>
                    </tr>
                </c:forEach>
            </table>
<%@include file="../shared/footer.jsp"%>

