<%@include file="../shared/header.jsp"%>
<div class="container">
    <h1>Add Customers</h1>
    <form method="post">
        <div class="form-group">
            <label>First Name</label>
            <input type="text" name="firstName" class="form-control"  required/>

        </div>
        <div class="form-group">
            <label>Last Name</label>
            <input type="text" name="lastName" class="form-control"  required/>

        </div>
        <div class="form-group">
            <label>Email address</label>
            <input type="email" name="email" class="form-control"  required/>
        </div>
        <div class="form-group">
            <label>Contact Number</label>
            <input type="integer" name="contactNo" class="form-control"  required/>
        </div>
        <div class="checkbox">
            <label>Status</label>
            <label><input type="checkbox" name="status" required/>Active</label>
        </div>
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
        <button type="submit" class="btn btn-success">Save</button>
        <a href="${BASE_URL}/admin/customer" class="btn btn-danger">
            Back
        </a>
</div>
</form>
<%@include file="../shared/footer.jsp"%>