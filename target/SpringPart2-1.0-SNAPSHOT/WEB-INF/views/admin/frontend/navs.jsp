

<nav class="navbar navbar-inverse">
    <div class="container-fluid">

        <ul class="nav navbar-nav">
            <li class="active"><a href="#">Customers</a></li>
            <li><a href="#">About</a></li>
            <li><a href="#">Contact</a></li>
        </ul>
        <form class="navbar-form navbar-right">
           
           <div class="pull-right">
                <p>
                    <a href="${BASE_URL}/admin/logout" class="btn btn-danger">
                        LogOut
                    </a>
                </p>
            </div>
        </form>
    </div>
</nav>
